import firebase from 'firebase/app';
import 'firebase/firestore'; // for the db
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyDdFubezxR9FHPdVuU9emax0oOqHeW_de0",
  authDomain: "nomad-bags-store-23278.firebaseapp.com",
  projectId: "nomad-bags-store-23278",
  storageBucket: "nomad-bags-store-23278.appspot.com",
  messagingSenderId: "704980600111",
  appId: "1:704980600111:web:6531cb0fe1cfa7c8da4dda"
};

firebase.initializeApp(config);

const firestore = firebase.firestore();
const auth = firebase.auth();

const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) { return };

  const userRef = firestore.doc(`users/${userAuth.uid}`) //users/uniq26535
  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      });
    } catch (error) {
      console.log('error creating user', error.message);
    }
  }

  return userRef;
}

export {
  firestore,
  createUserProfileDocument,
  auth,
}
